img1 = imread('input/ps0-a-1.png');
img2 = imread('input/ps0-a-2.png');
subplot(4,4,1), imshow(img1) ; title('Original');
subplot(4,4,5), imshow(img2) ; title('Original');

%% 2 

img1_g = img1;
img1_g (:,:,2) = 0;
subplot(4,4,2), imshow(img1_g); title('without green');

img1_swap = img1;
img1_swap(:,:,3) = img1(:,:,1);
img1_swap(:,:,1) = img1(:,:,3);
subplot(4,4,3), imshow(img1_swap); title('swapped green with blue');

img1_mono_g = img1;
img1_mono_g(:,:,[1 3])=0;
subplot(4,4,4), imshow(img1_mono_g); title('monochrome green image 1');

img1_mono_r = img1;
img1_mono_r(:,:,2:3) = 0;
subplot(4,4,5), imshow(img1_mono_r); title('monochrome red');

img1_center = size(img1)/2;
img1_center = int8(img1_center(1:2));
display(img1_center(1));
img1_square = img1_mono_g( (img1_center(1) - 49):(img1_center(1) + 50),(img1_center(2) - 49):(img1_center(2) + 50), :); 
subplot(4,4,6), imshow(img1_square); title('center image1');

img2_mono_g = img2;
img2_mono_g(:,:,[1 3])=0;
subplot(4,4,7), imshow(img2_mono_g); title('monochrome green image 2');

img_changed_center = img2_mono_g;
img_changed_center((img1_center(1) - 49):(img1_center(1) + 50),(img1_center(2) - 49):(img1_center(2) + 50), :)...
    = img1_square; 
subplot(4,4,8), imshow(img_changed_center); title('changed centre');




